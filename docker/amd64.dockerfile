FROM debian:bullseye-slim

# install dependencies
RUN apt-get update
RUN apt-get install -y --no-install-recommends wget tar xz-utils python3 python3-setuptools python3-pip python3-dev nodejs npm binutils rsync openssh-client patch autoconf automake  build-essential ccache cmake gettext git libffi-dev libltdl-dev libssl-dev libtool openjdk-11-jdk pkg-config sudo unzip zip zlib1g-dev zipalign moreutils
RUN rm -rf /var/lib/apt/lists/*

RUN pip3 --no-cache-dir install pyinstaller==4.10 buildozer Cython==0.29.19 wheel pip

ENV USER="user"
ENV HOME_DIR="/home/${USER}"
ENV PATH="${HOME_DIR}/.local/bin:${PATH}"
RUN useradd --create-home --shell /bin/bash ${USER}
RUN usermod -append --groups sudo ${USER}
RUN echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

WORKDIR ${HOME_DIR}
RUN chown -R ${USER}:${USER} .
USER ${USER}

RUN git clone https://framagit.org/basthon/basthon-desktop.git .basthon-desktop
WORKDIR .basthon-desktop/
RUN yes | buildozer android update
WORKDIR ${HOME_DIR}