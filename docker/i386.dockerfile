FROM i386/debian:bullseye-slim

# install dependencies
RUN apt-get update
RUN apt-get install -y --no-install-recommends wget tar xz-utils gnupg xvfb ca-certificates python3 python3-setuptools python3-pip python3-dev nodejs npm binutils rsync openssh-client patch

RUN pip3 --no-cache-dir install pyinstaller==4.10

# install wine
RUN echo deb https://dl.winehq.org/wine-builds/debian/ bullseye main >> /etc/apt/sources.list
RUN wget --no-check-certificate -nc https://dl.winehq.org/wine-builds/winehq.key
RUN apt-key add winehq.key
RUN apt-get update
RUN apt-get install -y --install-recommends winehq-stable

# remove cache
RUN rm -rf /var/lib/apt/lists/*

# get and install Python under X Virtual Frame Buffer
WORKDIR /root/.wine/drive_c
RUN echo "set -e\nexport DISPLAY=:1\nexport WINEDLLOVERRIDES='mscoree,mshtml='\nXvfb :1 &\nwget https://www.python.org/ftp/python/3.8.9/python-3.8.9.exe\nwine python-3.8.9.exe /quiet InstallAllUsers=1 PrependPath=1\nwine pip install pyinstaller" > install.sh
RUN chmod +x install.sh
RUN ./install.sh
