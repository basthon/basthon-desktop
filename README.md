# Basthon-Desktop

**This project has moved [here](https://forge.apps.education.fr/basthon/basthon-desktop)**.

Une version de bureau pour [Basthon](https://basthon.fr) ! Cette application est :

- **portable** (fonctionne sur Windows, GNU/Linux, macOS et Android)
- **simple d'utilisation** (sans installation, un seul fichier à placer où on veut, sur une clef USB par exemple)
- **autonome** (fonctionne sans accès à Internet)
- **légère** (la version minimale pèse ~30Mo)
- **libre** (le code est placé sous la [licence GNU GPL v3](https://framagit.org/basthon/basthon-desktop/-/blob/master/LICENSE) ou supérieure).

## Téléchargement

| OS                |                               Complète                               |                                Légère                                |                                Minimale                                |
| :---------------- | :------------------------------------------------------------------: | :------------------------------------------------------------------: | :--------------------------------------------------------------------: |
| GNU/Linux 32 bits | [⬇ ~280Mo](https://desktop.basthon.fr/latest?os=linux32&bundle=full) | [⬇ ~140Mo](https://desktop.basthon.fr/latest?os=linux32&bundle=lite) | [⬇ ~30Mo](https://desktop.basthon.fr/latest?os=linux32&bundle=minimal) |
| GNU/Linux 64 bits | [⬇ ~280Mo](https://desktop.basthon.fr/latest?os=linux64&bundle=full) | [⬇ ~140Mo](https://desktop.basthon.fr/latest?os=linux64&bundle=lite) | [⬇ ~30Mo](https://desktop.basthon.fr/latest?os=linux64&bundle=minimal) |
| Windows           | [⬇ ~280Mo](https://desktop.basthon.fr/latest?os=windows&bundle=full) | [⬇ ~140Mo](https://desktop.basthon.fr/latest?os=windows&bundle=lite) | [⬇ ~30Mo](https://desktop.basthon.fr/latest?os=windows&bundle=minimal) |
| macOS             |  [⬇ ~300Mo](https://desktop.basthon.fr/latest?os=macos&bundle=full)  |  [⬇ ~125Mo](https://desktop.basthon.fr/latest?os=macos&bundle=lite)  |  [⬇ ~30Mo](https://desktop.basthon.fr/latest?os=macos&bundle=minimal)  |
| Android (APK)     | [⬇ ~285Mo](https://desktop.basthon.fr/latest?os=android&bundle=full) | [⬇ ~145Mo](https://desktop.basthon.fr/latest?os=android&bundle=lite) | [⬇ ~40Mo](https://desktop.basthon.fr/latest?os=android&bundle=minimal) |

### Avant de se lancer

Vous pouvez regarder les vidéos de présentation :

- [Utilisation avec GNU/Linux](https://files.basthon.fr/desktop/demo_linux.mp4)
- [Utilisation avec Windows](https://files.basthon.fr/desktop/demo_windows.mp4)

Le détail de chaque version se trouve ci-dessous. Notez que :

- la taille du fichier impacte de manière significative le temps de chargement de l'application ;
- sous Windows, la présence d'un antivirus peut considérablement ralentir le chargement de l'application ;
- ⚠ sous GNU/Linux, il faudra rendre le fichier téléchargé exécutable.

#### Version complète

Elle contient tous les langages supportés dans Basthon ainsi que tous les modules Python.

#### Version légère

Elle contient tous les langages supportés dans Basthon mais les modules Python suivant ne sont pas disponibles :
`astropy, bokeh, Fiona, mne, mypy, opencv, pandas, pyproj, scikit-image, scikit-learn, scipy, statmodels, sympy, yt`.

#### Version minimale

Le langage OCaml n'est pas disponible et seuls les modules de la librairie standard de Python sont inclus.

<!-- modules restants : distlib, traits, future, regex, pyodide-interrupts, micropip, attrs -->
