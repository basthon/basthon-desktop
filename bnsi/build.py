#!/usr/bin/env python3

import sys
from pathlib import Path
import json
from urllib.request import urlopen, Request
import gzip

nsujets = 48


class BNSAPI:
    """A class to interact with BNS API (mainly to download files)."""

    def __init__(self):
        url_base = "https://cyclades.education.gouv.fr/delos/api"
        self._api_urls = {
            "bns": f"{url_base}/public/sujets/ece?itemsPerPage=100",
            "sujet": f"{url_base}/public/sujet/{{}}",
            "file": f"{url_base}/file/public/{{}}",
        }
        self.__bns = None
        self.__sujets = {}

    def _bns(self):
        """Get the main resource json file from API."""
        if self.__bns is None:
            with urlopen(self._api_urls["bns"]) as response:
                self.__bns = json.loads(response.read())
            assert self.__bns["totalElements"] == nsujets
            assert len(self.__bns["content"]) == self.__bns["totalElements"]
        return self.__bns

    def sujet_api(self, index):
        """Get the subject file from API (given its index)."""
        if index not in self.__sujets:
            bns = self._bns()
            sujets = bns["content"]
            sujet = sujets[index - 1]
            id = sujet["id"]
            with urlopen(self._api_urls["sujet"].format(id)) as response:
                self.__sujets[index] = json.loads(response.read())
        return self.__sujets[index]

    def download_files_from_api(self, index):
        """Download a file from a subject given by its index."""
        index = {45: 46, 46: 45}.get(index, index)
        sujet = self.sujet_api(index)
        assert len(sujet["fichiers"]) == 2
        for fichier in sujet["fichiers"]:
            url = self._api_urls["file"].format(fichier["id"])
            with urlopen(
                Request(url, headers={"Accept-Encoding": "gzip, deflate"})
            ) as response:
                data = response.read()
                info = response.info()
                if info.get("Content-Encoding") == "gzip":
                    data = gzip.decompress(data)
                filename = Path(
                    [
                        f.split("=")[-1]
                        for f in info.get("Content-Disposition").split(";")
                        if f.startswith("filename=")
                    ][0]
                )
            # fix broken filenames
            read_index = int(str(filename.stem).split("-")[2])
            if index != read_index:
                print(f"WARNING: le sujet {index} pointe sur le sujet {read_index}")
            filename = Path(f"24-NSI-{read_index:02d}{filename.suffix}")
            path = Path("sujets") / filename
            path.parent.mkdir(parents=True, exist_ok=True)
            path.write_bytes(data)

    def path_file(self, index, ext):
        """Get the path of a file given by its
        index and extension (py or pdf).
        Download the file if it does not exists."""
        return Path("sujets") / f"24-NSI-{index:02d}.{ext}"

    def path_py(self, index):
        """See path_file with ext='py'."""
        return self.path_file(index, "py")

    def path_pdf(self, index):
        """See path_file with ext='pdf'."""
        return self.path_file(index, "pdf")

    def read_file(self, index, ext):
        """Get the file (given by extension, py or pdf)
        of subject given by index.
        Download it if it does not exists."""
        data = self.path_file(index, ext).read_bytes()
        if ext == "py":
            try:
                data = data.decode("utf8")
            except UnicodeDecodeError:
                data = data.decode("latin1")
        return data

    def read_py(self, index):
        """See read_file with ext='py'."""
        return self.read_file(index, "py")

    def read_pdf(self, index):
        """See read_file with ext='pdf'."""
        return self.read_file(index, "pdf")


sujets = (
    [int(i) for i in sys.argv[1:]] if len(sys.argv) > 1 else list(range(1, nsujets + 1))
)

with open("template.json") as f:
    ipynb = json.load(f)

bns_api = BNSAPI()

for sujet in sujets:
    dest = Path("ipynbs")
    dest.mkdir(parents=True, exist_ok=True)
    dest = dest / f"{sujet:02d}.ipynb"
    if dest.is_file():
        continue
    bns_api.download_files_from_api(sujet)
    data = bns_api.read_py(sujet)
    href = bns_api.path_pdf(sujet)
    switch = {
        0: f"# Sujet n°{sujet}",
        1: f'from IPython.display import IFrame\n\nIFrame(src="{href}", width="100%", height=800)',
        2: f'<a href="{href}" target="_blank" rel="noopener">Ouvrir le sujet dans un nouvel onglet</a>\n\n## Exercice 1',
        5: data,
    }
    for i, c in enumerate(ipynb["cells"]):
        c["source"] = switch.get(i, c["source"])
    with dest.open("w") as f:
        json.dump(ipynb, f)
