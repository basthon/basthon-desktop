# This file is writen to be compatible with Pyhton3 and Python2.
# This is why some part may be ugly...
try:
    # only for APK deploy
    import android
except ImportError:
    pass
import threading
from http.server import SimpleHTTPRequestHandler as Handler
import socketserver
import webbrowser
import time
import sys
import os
import json

PYTHON2 = sys.version_info[0] == 2

if PYTHON2:
    input = raw_input

# get application path
if getattr(sys, 'frozen', False):
    application_path = sys._MEIPASS
else:
    application_path = os.path.dirname(os.path.abspath(__file__))


# the webserver instance
httpd = None
server_start_fail = False

# basthon's root path
root = os.path.join(application_path, "root")

# redirections file
with open(os.path.join(root, "redirections.json")) as f:
    redirections = json.load(f)


def redirect(path):
    # abandon query parameters
    path = path.split('?', 1)[0]
    path = path.split('#', 1)[0]
    if path.rstrip().endswith('/'):
        path = path + "index.html"
    for pattern, rootdir in redirections.items():
        pattern = "/" + pattern
        if (path.startswith(pattern)
            and (len(path) == len(pattern)
                 or path[len(pattern)] in ('/', '#', '?'))):
            return redirect("/" + rootdir + path[len(pattern):])
    return path


class MyHandler(Handler):
    def log_request(self, code='-', size='-'):
        port = self.server.socket.getsockname()[1]
        rl = self.requestline
        self.requestline = "port=" + str(port) + " " + rl
        super().log_request(code, size)
        self.requestline = rl

    def translate_path(self, path):
        return super().translate_path(redirect(path))


def start_server():
    global httpd
    global server_start_fail
    try:
        os.chdir(root)
        _httpd = socketserver.TCPServer(("", 0), MyHandler)
        port = _httpd.socket.getsockname()[1]
        print("Starting local webserver on port " + str(port) + ".")
        httpd = _httpd
        _httpd.serve_forever()
        _httpd.server_close()
    except Exception as error:
        server_start_fail = True
        raise error


# starting web server in separate thread
threading.Thread(target=start_server).start()

# waiting for server to be started
while httpd is None and not server_start_fail:
    time.sleep(0.1)

# get port and url
port = httpd.socket.getsockname()[1]
url = "http://localhost:" + str(port) + "/"
print("A web-browser should be automatically opened. "
      "Otherwise, look at " + url)

# opening URL in webbrowser
webbrowser.open_new_tab(url)

input("Press Enter to stop the server.\n")
httpd.shutdown()
