#!/usr/bin/env python3

import os
from urllib.request import urlretrieve as _urlretrieve
import tarfile
from pathlib import Path
import shutil
import argparse
import json
import fileinput
import platform

TARGETS = ("full", "lite", "minimal", "bnsi")
TARGETS = dict(zip(TARGETS, range(len(TARGETS))))
with open("package.json") as f:
    arch = platform.architecture()[0]
    VERSION = f"{json.load(f)['version']}-{arch}"


def mrproper(target):
    def rm(path):
        if path.is_file():
            path.unlink()
        else:
            shutil.rmtree(path, ignore_errors=True)

    def rm_empty(path):
        doit = True
        while doit:
            doit = False
            for f in list(path.rglob("")):
                try:
                    f.rmdir()
                    doit = True
                except Exception:
                    pass

    root = Path("app") / "root"
    with open(Path("app") / "mrproper.json") as f:
        mrproper = json.load(f)
    for tg, level in TARGETS.items():
        if level > TARGETS[target]:
            break
        mrp = mrproper[tg]
        for top, keep in mrp["include"].items():
            top = next(root.glob(top))
            all = set(f for f in top.rglob("*") if f.is_file())
            for f in all.difference(*(top.rglob(glob) for glob in keep)):
                rm(f)
            # remove remaining empty directories
            rm_empty(top)
        for glob in mrp["exclude"]:
            for f in root.rglob(glob):
                rm(f)

    # remove OCaml in minimal/bnsi modes
    if target in ("minimal", "bnsi"):
        for f in root.rglob("assets/*.js"):
            delete = False
            with open(f) as file:
                if "Js_of_ocaml" in file.read():
                    delete = True
            if delete:
                rm(f)

    # apply patch
    if (Path("app") / "patches" / f"{target}.patch").is_file():
        cwd = os.getcwd()
        os.chdir(root)
        assert os.system(f"patch -p2 -i ../patches/{target}.patch") == 0
        os.chdir(cwd)

    # add subjects in bnsi
    if target == "bnsi":
        os.chdir("bnsi")
        assert os.system("python3 build.py") == 0
        os.chdir("..")
        dest = root / "console" / "sujets"
        dest.mkdir(parents=True, exist_ok=True)
        # ipynbs
        for src_file in (Path("bnsi") / "ipynbs").glob("*.*"):
            shutil.copy(src_file, dest)
        # pdfs
        for src_file in (Path("bnsi") / "sujets").glob("*.*"):
            shutil.copy(src_file, dest)


def build_target(target):
    def urlretrieve(url, dest):
        tmp = Path("tmp")
        tmp.mkdir(parents=True, exist_ok=True)
        dest = tmp / dest
        if not dest.is_file():
            _urlretrieve(url, dest)
        return dest

    def sed(old, new, path):
        for line in fileinput.input(path, inplace=True):
            print(line.replace(old, new), end="")

    root = Path("app") / "root"
    shutil.rmtree(root, ignore_errors=True)
    # get portal
    portal = "basthon-fr.tgz"
    portal = urlretrieve(f"https://basthon.fr/{portal}", portal)
    with tarfile.open(portal, "r:gz") as tar:
        root.mkdir(parents=True, exist_ok=True)
        tar.extractall(root)
    for f in root.rglob("*.html"):
        if f.is_file():
            sed("https://console.basthon.fr", "console", f)
            sed("https://notebook.basthon.fr", "notebook", f)
    # get frontends
    for frontend in ("console", "notebook"):
        fname = f"basthon-{frontend}.tgz"
        fname = urlretrieve(f"https://{frontend}.basthon.fr/{fname}", fname)
        with tarfile.open(fname, "r:gz") as tar:
            dest = root / frontend
            dest.mkdir(parents=True, exist_ok=True)
            tar.extractall(dest)
    # get pyodide
    pyodide_url = f"https://pyodide.basthon.fr/latest/{target}/pyodide.tgz"
    pyodide_file = urlretrieve(pyodide_url, f"pyodide-{target}.tgz")
    # put pyodide in console
    with tarfile.open(pyodide_file, "r:gz") as tar:
        base = root / "console" / "assets"
        dest = [f for f in base.iterdir() if f.is_dir()][0] / "python3"
        dest.mkdir(parents=True, exist_ok=True)
        tar.extractall(dest)
    # symlink notebooks's pyodide to console
    nb_assets = root / "notebook" / "assets" / dest.parent.name
    shutil.rmtree(nb_assets, ignore_errors=True)
    nb_assets.symlink_to(
        Path("..") / ".." / "console" / "assets" / dest.parent.name,
        target_is_directory=True,
    )
    # in bnsi, sujets are in console
    if target == "bnsi":
        (root / "notebook" / "sujets").symlink_to(
            Path("..") / "console" / "sujets", target_is_directory=True
        )
    # construct redirections.json
    redirections = {}
    to_remove = []
    for f in root.rglob("*"):
        if f.is_symlink():
            src = f.relative_to(root)
            # follow symlinks
            dst = f.resolve().relative_to(root.resolve())
            redirections[str(src)] = str(dst)
            to_remove.append(f)
    # remove symlinks
    for f in to_remove:
        f.unlink()
    with open(root / "redirections.json", "w") as f:
        json.dump(redirections, f)
    mrproper(target)
    # effective build
    cmd = (
        'pyinstaller \
              --noconfirm \
              --exclude-module numpy \
              --onefile \
              --icon "assets/basthon.ico" \
              --console \
              --add-data "app/root:root" '
        f'--name basthon-desktop-{target}-{VERSION} "app/main.py"'
    )
    assert os.system(cmd) == 0
    if os.system("wine pyinstaller --help") == 0:
        cmd = cmd.replace(":", ";").replace("/", "\\")
        assert os.system(f"wine {cmd}") == 0
    if os.system("buildozer --version") == 0 and target != "bnsi":
        assert os.system(f"buildozer --profile {target} android release") == 0
        apk = list(Path("dist").glob(f"basthon_desktop_{target}-*.apk"))
        assert len(apk) == 1
        apk = apk[0]
        apk = apk.rename(str(apk).replace("_", "-"))
        assert (
            os.system(
                f"jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE -storepass $STOREPASS {apk} basthon"
            )
            == 0
        )
        assert (
            os.system(
                f"zipalign -v 4 {apk} {str(apk).replace('-release-unsigned', '')}"
            )
            == 0
        )
        apk.unlink()


def build():
    for target in TARGETS.keys():
        build_target(target)


def clean():
    for d in (Path("app") / "root", "tmp", "build", "dist"):
        shutil.rmtree(d, ignore_errors=True)
    for glob in ("basthon-desktop.spec", "pyodide-*.tgz"):
        for f in Path(".").glob(glob):
            f.unlink()


parser = argparse.ArgumentParser(description="Build Basthon-Desktop app.")
parser.add_argument(
    "action",
    type=str,
    nargs="?",
    default="build",
    help="The action to execute: build or clean (default: build)",
)
args = parser.parse_args()


globals()[args.action]()
