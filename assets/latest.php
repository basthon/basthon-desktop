<?php
$os = filter_input(INPUT_GET, 'os', FILTER_SANITIZE_STRING);
if (!in_array($os, array("windows", "linux32", "linux64", "macos", "android"))) {
    $os = 'windows';
}

$bundle = filter_input(INPUT_GET, 'bundle', FILTER_SANITIZE_STRING);
if (!in_array($bundle, array("full", "lite", "minimal", "bnsi"))) {
    $bundle = 'minimal';
}

// recover last version
$files = glob("../download/" . $os . "/basthon-desktop-" . $bundle . "-*");
$last  = "";
$last_version = "0.0.0";
foreach ($files as $file) {
    $version = explode("-", $file)[3];
    if (version_compare($version, $last_version) >= 0) {
        $last_version = $version;
        $last = $file;
    }
}

header("Location: https://" . $_SERVER['SERVER_NAME'] . str_replace("../", "/", $last));
exit;
?> 
